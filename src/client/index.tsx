import React from "react";
import ReactDOM from "react-dom";

import { CurrentUserProvider } from "./hooks/useCurrentUser";
import { RatesProvider } from "./hooks/useRates";
import { ModalProvider } from "./hooks/useModal";
import { App } from "./components/App/App";

let currentUser: UserType = {
  exchangePair: {
    source: "EUR",
    target: "USD",
  },
  pockets: [
    { currency: "EUR", symbol: "€", balance: 100 },
    { currency: "USD", symbol: "$", balance: 50 },
    { currency: "GBP", symbol: "£", balance: 10 },
  ],
};

ReactDOM.render(
  <ModalProvider>
    <CurrentUserProvider currentUser={currentUser}>
      <RatesProvider>
        <App />
      </RatesProvider>
    </CurrentUserProvider>
  </ModalProvider>,
  document.getElementById("app")
);
