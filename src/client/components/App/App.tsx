import React from "react";

import { Modal } from "@components/Modal/Modal";
import { Exchange } from "@components/Exchange/Exchange";

import { globalStyles } from "./global.styles";
import { styles } from "./App.styles";

export const App = () => {
  return (
    <div className="App">
      <style jsx>{globalStyles}</style>
      <style jsx>{styles}</style>

      <div className="App-content">
        <Exchange />
      </div>

      <Modal />
    </div>
  );
};
