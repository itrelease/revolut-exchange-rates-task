import css from "styled-jsx/css";

export const styles = css`
  .App {
    display: flex;
    flex: 1;
  }

  .App-content {
    max-width: 768px;
    margin: 0 auto;
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
`;
