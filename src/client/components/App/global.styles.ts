import css from "styled-jsx/css";

export const globalStyles = css.global`
  * {
    box-sizing: border-box;
  }

  html,
  body,
  #__next {
    width: 100%;
    height: 100vh;
    height: -webkit-fill-available;
    font-family: "Inter", sans-serif;
    background-color: #fff;
  }

  body,
  #app {
    display: flex;
  }

  input,
  select {
    font-size: 100%;
  }

  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  input[type="number"] {
    -moz-appearance: textfield;
  }

  body,
  p,
  h1,
  h2,
  h3,
  h4 {
    margin: 0;
  }

  #app {
    flex: 1;
  }
`;
