import React, { useCallback, useEffect, useRef, useState } from "react";

import { useCurrentUser } from "@hooks/useCurrentUser";
import { useRates } from "@hooks/useRates";
import { useModal } from "@hooks/useModal";
import { Button } from "@components/Button/Button";

import { RateInfo } from "./RateInfo";
import { styles } from "./Exchange.styles";

export const Exchange = () => {
  const modal = useModal();
  const {
    currentUser: { pockets, exchangePair },
    updateExchangePair,
    updatePockets,
  } = useCurrentUser();
  const rates = useRates();
  const [amount, setAmount] = useState({ source: "", target: "" });
  const leaderInput = useRef("source");
  const sourcePocket = pockets.find((p) => p.currency === exchangePair.source);
  const targetPocket = pockets.find((p) => p.currency === exchangePair.target);

  const calculateTargetAmount = (sourceAmount, rates) => {
    const targetAmount = (
      Number(sourceAmount) * rates.values[targetPocket.currency]
    ).toFixed(2);

    return String(targetAmount);
  };

  const calculateSourceAmount = (targetAmount, rates) => {
    const sourceAmount = (
      Number(targetAmount) / rates.values[targetPocket.currency]
    ).toFixed(2);

    return String(sourceAmount);
  };

  const handleSelectCurrency = (event) => {
    modal.open({
      type: "SELECT_CURRENCY",
      payload: {
        type: event.currentTarget.dataset.type,
      },
    });
  };

  const handleSwitchClick = useCallback(() => {
    updateExchangePair(targetPocket.currency, sourcePocket.currency);
  }, [sourcePocket, targetPocket]);

  const handleAmountChange = (event) => {
    const inputType = event.target.dataset.type;
    const value = event.target.value || "";
    const regex = /^(\d+)([.,]{0,1})(\d*)$/;

    leaderInput.current = inputType;

    if (!regex.test(value) && value !== "") {
      return;
    }

    const match = regex.exec(value);
    let amount = value;

    if (match && (match[3] || "").length > 2) {
      amount = match[1] + match[2] + match[3].slice(0, 2);
    }

    if (inputType === "source") {
      setAmount({
        source: amount,
        target: !amount ? "" : calculateTargetAmount(amount || 0, rates),
      });
    } else {
      setAmount({
        target: amount,
        source: !amount ? "" : calculateSourceAmount(amount || 0, rates),
      });
    }
  };

  const handleExchangeClick = () => {
    const sourceAmount = Number(amount.source);
    const targetAmount = Number(amount.target);

    const pockets = {
      [sourcePocket.currency]: {
        ...sourcePocket,
        balance: +(sourcePocket.balance - sourceAmount).toFixed(2),
      },

      [targetPocket.currency]: {
        ...targetPocket,
        balance: +(targetPocket.balance + targetAmount).toFixed(2),
      },
    };

    setAmount({
      source: "",
      target: "",
    });

    updatePockets(pockets);
  };

  let warning = null;

  if (Number(amount.source) > sourcePocket.balance) {
    warning = "exceedsBalance";
  } else if (amount.source.length > 0 && Number(amount.source) < 0.1) {
    warning = "minimumAmount";
  }

  useEffect(() => {
    return () => {
      rates.updateBase(null);
    };
  }, []);

  // Fetch rates for new source currency
  useEffect(() => {
    rates.updateBase(sourcePocket.currency);
  }, [sourcePocket.currency]);

  // Recalculate amount when rates or pockets changed
  useEffect(() => {
    if (rates.type === "unknown" || !rates.values[targetPocket.currency]) {
      return;
    }

    if (leaderInput.current === "source") {
      setAmount({
        source: amount.source,
        target: !amount.source
          ? ""
          : calculateTargetAmount(amount.source || 0, rates),
      });
    } else {
      setAmount({
        target: amount.target,
        source: !amount.target
          ? ""
          : calculateSourceAmount(amount.target || 0, rates),
      });
    }
  }, [rates, sourcePocket, targetPocket]);

  return (
    <div className="Exchange">
      <style jsx>{styles}</style>

      <div className="Exchange-info">
        <RateInfo
          sourceSymbol={sourcePocket.symbol}
          targetSymbol={targetPocket.symbol}
          value={rates.type === "ready" && rates.values[targetPocket.currency]}
        />
      </div>

      {/* Source input */}
      <div className="Exchange-block" data-type="source">
        <div
          className="Exchange-currency"
          data-type="source"
          onClick={handleSelectCurrency}
        >
          <div>
            {sourcePocket.currency}{" "}
            <img
              src="/public/icons/arrow-down.svg"
              width="18"
              alt="select source currency"
            />
          </div>

          <span className="Exchange-meta">
            Balance: {sourcePocket.balance} {sourcePocket.symbol}
          </span>
        </div>

        <div className="Exchange-amount">
          <div className="Exchange-inputContainer">
            {amount.source.length > 0 && (
              <span className="Exchange-sign">-</span>
            )}

            <input
              className="Exchange-input"
              type="number"
              value={amount.source || ""}
              placeholder="0"
              disabled={rates.type === "unknown"}
              data-type="source"
              onChange={handleAmountChange}
            />
          </div>

          {warning === "exceedsBalance" && (
            <span className="Exchange-meta">exceeds balance</span>
          )}

          {warning === "minimumAmount" && (
            <span className="Exchange-meta" data-error="true">
              Minimum amount is 0,10 {sourcePocket.symbol}
            </span>
          )}
        </div>

        <div className="Exchange-switchButton">
          <Button type="stroke" color="dark" onClick={handleSwitchClick}>
            <img
              className="Exchange-switchIcon"
              src="/public/icons/change.svg"
              width="12"
              height="12"
              alt="switch"
            />
          </Button>
        </div>
      </div>

      {/* Target input */}
      <div className="Exchange-block" data-type="target">
        <div
          className="Exchange-currency"
          data-type="target"
          onClick={handleSelectCurrency}
        >
          <div>
            {targetPocket.currency}{" "}
            <img
              src="/public/icons/arrow-down.svg"
              width="18"
              alt="select target currency"
            />
          </div>

          <span className="Exchange-meta">
            Balance: {targetPocket.balance} {targetPocket.symbol}
          </span>
        </div>

        <div className="Exchange-amount">
          <div className="Exchange-inputContainer">
            {amount.target.length > 0 && (
              <span className="Exchange-sign">+</span>
            )}

            <input
              className="Exchange-input"
              type="number"
              value={amount.target || ""}
              placeholder="0"
              data-type="target"
              disabled={rates.type === "unknown"}
              onChange={handleAmountChange}
            />
          </div>
        </div>
      </div>

      <div className="Exchange-button">
        <Button
          type="fill"
          color="blue"
          disabled={!amount.source || warning !== null}
          onClick={handleExchangeClick}
        >
          Exchange
        </Button>
      </div>
    </div>
  );
};
