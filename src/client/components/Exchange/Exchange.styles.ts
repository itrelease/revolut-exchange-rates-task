import css from "styled-jsx/css";

export const styles = css`
  .Exchange {
    width: 100%;
  }

  .Exchange-block {
    position: relative;
    display: flex;
    padding: 32px 42px;
  }

  .Exchange-block[data-type="source"]:after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0;
    height: 1px;
    width: 100%;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      #1e90ff,
      rgba(0, 0, 0, 0) 100%
    );
  }

  .Exchange-currency,
  .Exchange-amount {
    display: flex;
    flex-direction: column;
    flex: 1;
    font-size: 32px;
  }

  .Exchange-currency {
    cursor: pointer;
    font-weight: 600;
  }

  .Exchange-inputContainer {
    position: relative;
    display: flex;
  }

  .Exchange-sign {
    position: absolute;
    transform: translateX(-150%);
  }

  .Exchange-input {
    appearance: none;
    border: none;
    background-color: transparent;
    padding: 0;
    flex: 1;
    outline: none;
    width: 100%;
  }

  .Exchange-input:disabled {
    color: #000;
  }

  .Exchange-meta {
    margin-top: 8px;
    font-size: 14px;
    color: #969696;
    font-weight: 100;
  }

  .Exchange-meta[data-error="true"] {
    color: red;
  }

  .Exchange-info,
  .Exchange-button {
    display: flex;
    justify-content: center;
  }

  .Exchange-switchButton {
    position: absolute;
    bottom: 0;
    transform: translateY(50%);
    background: #fff;
    z-index: 1;
    left: 56px;
  }

  .Exchange-switchIcon {
    transform: rotate(90deg);
  }
`;
