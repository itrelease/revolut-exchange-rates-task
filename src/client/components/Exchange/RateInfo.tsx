import React from "react";

import { styles } from "./RateInfo.styles";

type PropsType = {
  sourceSymbol: string;
  targetSymbol: string;
  value?: number;
};

export const RateInfo = React.memo(
  ({ sourceSymbol, targetSymbol, value }: PropsType) => {
    const isLoading = typeof value !== "number";

    return (
      <div className="RateInfo" data-loading={isLoading}>
        <style jsx>{styles}</style>

        {isLoading && (
          <img
            src="/public/icons/update.svg"
            width="12"
            height="12"
            alt="loading"
          />
        )}

        {!isLoading && (
          <span>
            1 {sourceSymbol} = {value} {targetSymbol}
          </span>
        )}
      </div>
    );
  }
);
