import css from "styled-jsx/css";

export const styles = css`
  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  .RateInfo {
    border: 1px solid #1e90ff;
    border-radius: 24px;
    font-size: 14px;
    color: #1e90ff;
    height: 30px;
    line-height: 30px;
    padding: 0 8px;
  }

  .RateInfo[data-loading="true"] {
    font-size: 1em;
    border-color: #000;
    border-radius: 30px;
    line-height: initial;
    padding: 4px 8px;
  }

  .RateInfo[data-loading="true"] img {
    animation: spin 1s infinite linear;
  }
`;
