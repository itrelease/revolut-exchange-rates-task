import css from "styled-jsx/css";

export const styles = css`
  .SelectCurrencyModal {
    width: 300px;
  }

  .SelectCurrencyModal-header {
    margin-bottom: 18px;
  }

  .SelectCurrencyModal-list {
    list-style: none;
    padding: 0;
    margin: 0;
  }

  .SelectCurrencyModal-listItem {
    display: flex;
    align-items: center;
    margin-bottom: 12px;
    cursor: pointer;
  }

  .SelectCurrencyModal-listItem img {
    margin-right: 8px;
  }

  .SelectCurrencyModal-listItem:last-child {
    margin-bottom: 0;
  }
`;
