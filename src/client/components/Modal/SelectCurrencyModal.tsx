import React from "react";

import { useCurrentUser } from "@hooks/useCurrentUser";

import { styles } from "./SelectCurrencyModal.styles";

type PropsType = {
  type: "source" | "target";
  onClose: () => void;
};

export const SelectCurrencyModal = (props: PropsType) => {
  const {
    currentUser: { pockets, exchangePair },
    updateExchangePair,
  } = useCurrentUser();
  const skipCurrency =
    props.type === "source" ? exchangePair.source : exchangePair.target;

  const handleSelect = ({ currentTarget }) => {
    const currency = currentTarget.dataset.currency;
    const newPair = {
      ...exchangePair,
    };

    if (props.type === "source") {
      newPair.source = currency;

      // just switching
      if (newPair.source === newPair.target) {
        newPair.target = exchangePair.source;
      }
    } else {
      newPair.target = currency;

      // just switching
      if (newPair.target === newPair.source) {
        newPair.source = exchangePair.target;
      }
    }

    updateExchangePair(newPair.source, newPair.target);

    props.onClose();
  };

  return (
    <div className="SelectCurrencyModal">
      <style jsx>{styles}</style>

      <h3 className="SelectCurrencyModal-header">Select currency</h3>

      <ul className="SelectCurrencyModal-list">
        {pockets
          .filter((p) => p.currency !== skipCurrency)
          .map((p) => {
            return (
              <li
                key={p.currency}
                className="SelectCurrencyModal-listItem"
                data-currency={p.currency}
                onClick={handleSelect}
              >
                <img
                  src={`/public/icons/currency/${p.currency}.svg`}
                  alt={p.currency}
                  width="32"
                />
                {p.currency}
              </li>
            );
          })}
      </ul>
    </div>
  );
};
