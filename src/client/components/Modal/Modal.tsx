import React, { useCallback, useEffect } from "react";

import { useModal } from "@hooks/useModal";

import { SelectCurrencyModal } from "./SelectCurrencyModal";
import { styles } from "./Modal.styles";

const ESC_KEY = 27;

export const Modal = () => {
  const { modal, status, close } = useModal();
  const handleKeydown = useCallback((event) => {
    if (event.keyCode === ESC_KEY) {
      close();
    }
  }, []);

  useEffect(() => {
    window.addEventListener("keydown", handleKeydown, false);

    return () => {
      window.removeEventListener("keydown", handleKeydown, false);
    };
  }, []);

  if (status === "closed") {
    return null;
  }

  let content = null;

  switch (modal.type) {
    case "SELECT_CURRENCY": {
      content = (
        <SelectCurrencyModal type={modal.payload.type} onClose={close} />
      );
      break;
    }

    default: {
      throw new Error(`Unknown modal type: ${modal.type}`);
    }
  }

  return (
    <div className="Modal" data-status={status}>
      <style jsx>{styles}</style>

      <div className="Modal-content">
        <div className="Modal-close">
          <img
            src="/public/icons/close.svg"
            width="12"
            alt="close"
            onClick={close}
          />
        </div>

        {content}
      </div>
    </div>
  );
};
