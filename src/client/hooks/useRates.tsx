import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";

type RatesContextType =
  | { type: "unknown"; updateBase: (base: CurrencyType) => void }
  | {
      type: "ready";
      base: CurrencyType;
      values: {
        [key in CurrencyType]: number;
      };
      updateBase: (base: CurrencyType) => void;
    };

const REFRESH_RATE = 10 * 1000; // 10 seconds

const RatesContext = React.createContext<RatesContextType>(null);

const RatesProvider: FunctionComponent = ({ children }) => {
  const [currentBase, setCurrentBase] = useState(null);
  const [rates, setRates] = useState({});
  const ratesForCurrentBase = currentBase ? rates[currentBase] : null;

  const handleUpdateBase = useCallback(
    (base) => {
      setCurrentBase(base);
    },
    [setCurrentBase]
  );
  const timeoutRef = useRef<NodeJS.Timeout>();
  const fetchRates = () => {
    clearTimeout(timeoutRef.current);

    // https://api.exchangeratesapi.io/latest?symbols=GBP&base=EUR
    window
      .fetch(`https://api.exchangeratesapi.io/latest?base=${currentBase}`)
      .then<API.ExchangeRatesType>((response) => response.json())
      .then((response) => {
        setRates({
          ...rates,
          [response.base]: response.rates,
        });
        timeoutRef.current = setTimeout(fetchRates, REFRESH_RATE);
      })
      .catch((error) => {
        console.warn(error);

        // retry
        fetchRates();
      });
  };

  useEffect(() => {
    if (currentBase) {
      fetchRates();
    } else {
      clearTimeout(timeoutRef.current);
    }
  }, [currentBase]);

  const value: RatesContextType =
    currentBase && ratesForCurrentBase
      ? {
          type: "ready",
          base: currentBase,
          values: ratesForCurrentBase,
          updateBase: handleUpdateBase,
        }
      : {
          type: "unknown",
          updateBase: handleUpdateBase,
        };

  return (
    <RatesContext.Provider value={value}>{children}</RatesContext.Provider>
  );
};

const useRates = () => {
  return useContext(RatesContext);
};

export { RatesContext, RatesProvider, useRates };
