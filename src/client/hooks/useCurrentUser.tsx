import React, { FunctionComponent, useContext, useState } from "react";

type PropsType = {
  currentUser: UserType;
};

const CurrentUserContext = React.createContext<{
  currentUser: UserType;
  updateExchangePair: (source: CurrencyType, target: CurrencyType) => void;
  updatePockets: (pockets: { [key in string]: PocketType }) => void;
}>(null);

const useCurrentUser = () => {
  return useContext(CurrentUserContext);
};

const CurrentUserProvider: FunctionComponent<PropsType> = ({
  currentUser,
  children,
}) => {
  const [_currentUser, setCurrentUser] = useState<UserType>(currentUser);
  const updateExchangePair = (source: CurrencyType, target: CurrencyType) => {
    const newCurrentUser: UserType = {
      ..._currentUser,
      exchangePair: {
        source,
        target,
      },
    };

    setCurrentUser(newCurrentUser);
  };
  const updatePockets = (pockets: { [key in string]: PocketType }) => {
    const newCurrentUser = {
      ..._currentUser,
      pockets: _currentUser.pockets.map((p) => pockets[p.currency] || p),
    };

    setCurrentUser(newCurrentUser);
  };
  const value = {
    currentUser: _currentUser,
    updateExchangePair,
    updatePockets,
  };

  return (
    <CurrentUserContext.Provider value={value}>
      {children}
    </CurrentUserContext.Provider>
  );
};

export { CurrentUserContext, CurrentUserProvider, useCurrentUser };
