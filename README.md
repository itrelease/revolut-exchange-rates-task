# Frontend Test

- `yarn` to install deps
- `yarn dev` to run it locally

## Main libraries used:

- `React` and `ReactDOM`
- `styled-jsx` - for styling

## Structure:

- `components/` - contains reusable parts
- `hooks/` - custom hooks like `useRates` or `useModal`
