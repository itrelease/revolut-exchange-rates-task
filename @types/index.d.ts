type ModalType = {
  type: "SELECT_CURRENCY";
  payload: {
    type: "source" | "target";
  };
};

type ModalStatusType = "closed" | "closing" | "opening" | "opened";

type CurrencyType = "EUR" | "USD" | "GBP";

type PocketType = {
  symbol: string;
  currency: CurrencyType;
  balance: number;
};

type UserType = {
  exchangePair: {
    source: CurrencyType;
    target: CurrencyType;
  };
  pockets: Array<PocketType>;
};

namespace API {
  type ExchangeRatesType = {
    base: CurrencyType;
    rates: {
      [key in CurrencyType]: number;
    };
  };
}
